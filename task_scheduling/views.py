from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Task
from .forms import AddTaskForm
from django.urls import reverse
from datetime import datetime

def index(request):
    all_tasks = Task.objects.all().values()
    response = {'tasks' : all_tasks}
    return render(request, 'home.html', response)

def add_task(request):
    form = AddTaskForm(request.POST or None)

    if(request.method =='POST' and form.is_valid):
        form.save()
        return HttpResponseRedirect(reverse('index'))
    
    form.base_fields['due_datetime'].label = 'Deadline'
    return render(request, 'add_task.html', {'form' : form})

def detail_task(request, pk):
    task = Task.objects.filter(id=pk).values()[0]
    # datetime_now = datetime.now()
    # due_datetime = task['due_datetime']
    # is_on_going = False
    # # timestamp_left = due_datetime - datetime_now
    # print(datetime_now, due_datetime)

    response = {'task' : task}
    return render(request, 'task_detail.html', response)

def delete_task(request, pk):
    Task.objects.filter(id=pk).delete()
    return HttpResponseRedirect(reverse('index'))

