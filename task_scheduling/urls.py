from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('add-task/', add_task, name='add_task'),
    path('detail-task/<int:pk>/', detail_task, name='detail_task'),
    path('delete-task/<int:pk>/', delete_task, name='delete_task'),
]