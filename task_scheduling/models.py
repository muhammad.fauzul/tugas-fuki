from datetime import datetime
from unicodedata import name
from django.db import models

class Task(models.Model):
    name = models.CharField(max_length=30)
    due_datetime = models.DateTimeField(default=datetime.now, blank=True)
    description = models.TextField()
