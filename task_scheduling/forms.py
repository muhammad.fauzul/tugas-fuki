from django import forms
from .models import Task
from datetime import datetime

class DateTimePickerInput(forms.DateTimeInput):
    input_type = 'datetime'

class AddTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'due_datetime', 'description']
    
    name = forms.CharField(label='Nama Tugas', max_length=30, required=True)
    due_datetime = forms.DateTimeField(widget=DateTimePickerInput, required=True)
    description = forms.Textarea()
